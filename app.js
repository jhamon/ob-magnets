var express    = require('express');
var app        = express();
var jade       = require('jade');
var path       = require('path');
var bodyParser = require('body-parser');

var monk  = require('monk');
var db    = monk('localhost:11111/ob-magnets');

var server = require('http').createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket){
  socket.on('user:join', function() {
    socket.broadcast.emit('user:joined', {viewers: io.engine.clientsCount});
  });

  socket.on('disconnect', function(){
    socket.broadcast.emit('user:left', {viewers: io.engine.clientsCount});
  });

  socket.on('magnet:drag', function(data) {
    db.get("words").update({_id: data._id}, {$set: data});
    socket.broadcast.emit('magnet:dragged', data);
  });

  socket.on('magnet:mousedown', function(id) {
    socket.broadcast.emit('magnet:mousedown', id);
  });
  socket.on('magnet:mouseup', function(id) {
    socket.broadcast.emit('magnet:mouseup', id);
  });
});

setTimeout(function() {
  io.emit("server:update");
}, 5000);

app.use(function (req, res, next) {
  req.db = db;
  next();
});

// Asset configuration
var staticDir = path.join(__dirname, 'public');
app.use(express.static(staticDir));

// Incoming request config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// View template engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/', function (req, res) {
  req.db.get("words").find({}, {}, function (e, docs) {
    res.render('index', {words: docs, viewerCount: io.engine.clientsCount});
  });
});

server.listen(3001, function () {
  console.log('Listening on port 3001');
});

module.exports = app;