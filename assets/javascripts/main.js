var $ = jQuery = require('jquery');
var draggabilly = require('../vendor/draggabilly.min.js');
var debounce    = require('./debounce.js');
require('./twitter.js')();

var draggies = {};
var socket;

function updateViewers(data) {
  $('.footer').text(data.viewers + ' viewing this fridge');
}

function disableWord(id) {
  var word = draggies[id];
  word.addClass('remote-click');
}

function enableWord(id) {
  var word = draggies[id];
  word.removeClass('remote-click');
}

function moveWord(data) {
  var word = draggies[data._id];
  word.addClass('remote-move')
      .css("left", data.x)
      .css("top", data.y);
  setTimeout(function () {
    word.removeClass('remote-move');
  }, 510);
}

function dragstopFn(e) {
  var clickedWord = $(e.target);
  var data        = {
    _id: clickedWord.data('id'),
    x: parseInt(clickedWord.css('left').slice(0, -2)),
    y: parseInt(clickedWord.css('top').slice(0, -2))
  };
  socket.emit('magnet:drag', data);
}

var data = bootstrappedData; // injected at template render
var starting    = Date.now();

var draggieList = [];
data.forEach(function (magnetData) {
  var magnet;

  if (magnetData.type === 'plain') {
    magnet = $('<div>')
        .addClass("text")
        .text(magnetData.text)
  } else if (magnetData.type === "image") {
    magnet = $('<img>')
        .attr("src", magnetData.text);
  } else if (magnetData.type === "alphabet") {
    magnet = $('<div>')
        .addClass("alphabet")
        .text(magnetData.text);
  }

  var draggie = $('<div>')
          .addClass('draggie')
          .data("id", magnetData._id)
          .css({"left": magnetData.x, "top": magnetData.y})
          .draggabilly({})
          .append(magnet)
      ;
  draggies[magnetData._id] = draggie;
  draggieList.push(draggie);
});
$('body').append(draggieList);
console.log('elapsed', starting - Date.now());

var io = require('socket.io-client');
socket = io();
socket.on('user:joined', updateViewers);
socket.on('user:left', updateViewers);
socket.on('magnet:dragged', moveWord);
socket.on('magnet:mousedown', disableWord);
socket.on('magnet:mouseup', enableWord);
socket.emit('user:join');
socket.on('server:update', function() {
  console.log('reloading!');
  window.location.reload(true);
});

draggieList.forEach(function (draggie) {
  draggie.on('dragEnd', dragstopFn);
  draggie.on('dragMove', debounce(dragstopFn, 100, false));
  draggie.on('pointerDown', function () {
    socket.emit('magnet:mousedown', draggie.data().id);
  });
  draggie.on('pointerUp', function () {
    socket.emit('magnet:mouseup', draggie.data().id);
  });
});