var Labels = {
  Delphine: "Delphine",
  Cosima: "Cosima",
  Sarah: "Sarah",
  Helena: "Helena",
  Rachel: "Rachel",
  Alison: "Alison",
  Felix: "Felix",
  Paul: "Paul",
  Leekie: "Leekie",
  Scott: "Scott",
  Frankenfics: "Frankenfics"
};

var sentences = {
  "I would like to see these mangoes": Labels.Helena,
  "Dude, you have killer grades": Labels.Cosima,
  "You're about to become a craven addict": Labels.Cosima,
  "I think I already am": Labels.Delphine,
  "Please, let me say goodbye to Cosima": Labels.Delphine,
  "My sexuality is not the most interesting thing about me": Labels.Cosima,
  //"I will erase you completely Rachel": Labels.Delphine,
  //"Fetch me something gay": Labels.Felix,
  "If you let her die without me it is personal": Labels.Delphine,
  "My security concerns are not jealousy": Labels.Delphine,
  "I'm French we enjoy lovers": Labels.Delphine,
  "These I like": Labels.Helena,
  "That complex": Labels.Cosima,
  "You have to love all of us": Labels.Cosima,
  //"It was never Beth I loved": Labels.Paul,
  //"Sexuality is a spectrum": Labels.Delphine,
  "That's oddly romantic": Labels.Cosima,
  //"I think he took something from inside of me": Labels.Helena,
  //"It is good to see you again sestra": Labels.Helena,
  //"Let do lunch": Labels.Helena,
  "What am I looking for": Labels.Cosima,
  "Not love darling a scratching post": Labels.Felix,
  "The one who stays in your heart forever": Labels.Felix,
  "I'm sorry I made you make those hard choices and then blame you for them": Labels.Cosima,
  "I'm kind of always late so I'm kind of always sorry": Labels.Cosima,
  //"You cannot hide in minimalist furniture": Labels.Felix,
  "I learned from Dyad that secrets are power": Labels.Cosima,
  //"I could beat her like a French meringue ": Labels.Alison,
  //"This I like": Labels.Helena,
  "Mmmmm but his mind is sexy": Labels.Delphine,
  "I didn't want to fall for you": Labels.Delphine,
  "But I have": Labels.Delphine,
  "Do you want horse baby or sheep baby": Labels.Helena,
  "You make me cry sestra": Labels.Helena,
  //"Much legrooms": Labels.Helena,
  "What the baguette": Labels.CloneClub,
  "Dear": Labels.CloneClub,
  "I just want to make crazy science with you in our new lab": Labels.Delphine,
  "I can't stop thinking about that kiss": Labels.Delphine,
  "Enchantée I'm Delphine": Labels.Delphine,
  "Full disclosure I did peek": Labels.Cosima,
  "host parasite relationships": Labels.Delphine,
  "evodevo evolutionary development": Labels.Cosima,
  "As a Darwinist I thought it would interest you": Labels.Delphine,
  "I am so glad you came you won't be disappoint": Labels.Delphine,
  "A philosophy of today for tomorrow": Labels.Leekie,
  "Let's steal some bikes": Labels.Cosima,
  "I am going to get you so baked one day": Labels.Cosima,
  "I'm kind of always late so I'm kind of always sorry": Labels.Cosima,
  //"I'm early": Labels.Delphine,
  "should have left him when I left Paris": Labels.Delphine,
  //"he was supposed to follow next month": Labels.Delphine,
  "Yeah cold turkey it's the only way to go": Labels.Cosima,
  "after a jogging like this we like to smoke a nice little cigarette": Labels.Delphine,
  "really nice to make a friend in the Brave New World": Labels.Delphine,
  "should we invite him": Labels.Delphine,
  "you're single now": Labels.Cosima,
  "we've create a pluripotent stem cell line from human baby teeth": Labels.Leekie,
  "you're patenting transgenic embryos": Labels.Cosima,
  "I did some digging and then just guessed": Labels.Cosima,
  "she's very cheeky this girl but that's why I like her": Labels.Delphine,
  "applying to the institute": Labels.Leekie,
  "have a very unique perspective": Labels.Leekie,
  "every rule needs to be broken": Labels.Leekie,
  "I hope I am not disturbing you": Labels.Delphine,
  "it's just that i've never": Labels.Delphine,
  "bonsoir ma cherie": Labels.Delphine,
  "you're not gay": Labels.Cosima,
  "I'm a total idiot": Labels.Cosima,
  "Delphine Cormier not Beraud": Labels.Cosima,
  "I am so sorry": Labels.Delphine,
  "I'm so stupid": Labels.Cosima,
  "fake boyfriend in Paris": Labels.Cosima,
  "let me tell you this one thing I didn't want to fall for you I wasn't supposed to": Labels.Delphine,
  "but I have": Labels.Delphine,
  "yeah it showed": Labels.Cosima,
  "I've never been with a woman before": Labels.Delphine,
  "because you feel it": Labels.Delphine,
  "I'm sick Delphine": Labels.Cosima,
  "I'm on your side now please believe me you need to trust someone": Labels.Delphine,
  "I think the sequence is a message": Labels.Cosima,
  "I knew it was bullshit but I still thought you were on my side": Labels.Cosima,
  "I wanted to trust you": Labels.Cosima,
  "you're in danger": Labels.Delphine,
  "the real danger": Labels.Cosima,
  //"encrypted ID tag four nucleotides to work with thousands of permutations 324B21 molecular encoding": Labels.Delphine,
  "where are you going": Labels.Delphines,
  "I just want to make like crazy science with you": Labels.Delphine,
  "transgenic organ transplants": Labels.Delphine,
  "extrapolation of murine models": Labels.Cosima,
  "prepare yourself you're about to become a craven addict": Labels.Cosima,
  "I am never this hungry": Labels.Delphine,
  "derivative genetic material restricted intellectual property": Labels.Cosima,
  "I could kill for some icecream": Labels.Delphine,
  "are you okay": Labels.Delphine,
  "I cry after sex with boys too": Labels.Delphine,
  "poor you": Labels.Cosima,
  "but you know what": Labels.Delphine,
  "mmmmm your wish is my command": Labels.Cosima,
  "sequenced and analyzed": Labels.Scott,
  "distinct in each sample": Labels.Scott,
  "I was wondering if I could come over later": Labels.Delphine,
  "I'm gonna go to the store and get us some eskimo pies": Labels.Cosima,
  "I think I already am": Labels.Delphine,
  "it's really really good to finally meet someone who gets it who gets me": Labels.Delphine,
  "yeah ditto obvs": Labels.Cosima,
  "I can't stop thinking about that kiss": Labels.Delphine,
  "merde": Labels.Delphine,
  "like in a not bad way": Labels.Delphine,
  "I have never thought about bisexuality for myself you know but as a scientist I know that sexuality is a spectrum but social biases they codify attraction contrary to the biological facts you know": Labels.Delphine,
  "that's oddly romantic and totally encouraging": Labels.Delphine,
  "don't you think it's time we admit what this is really about": Labels.Cosima,
  "your glasses make you somewhat platonic": Labels.Leekie,
  "self-directed evolution": Labels.Leekie,
  "silver-grey hair and one white eye": Labels.Leekie,
  "Neolution is not eugenical it's neotopian": Labels.Delphine,
  "dandelion tattoo transgressive lesbian geek spiral": Labels.Cosima,
  "smut fluff angst clorgy": Labels.Frankenfics,
  "ballpit of denial blew the roofer at the cabin tasty Tony you": Labels.Frankenfics,
  "can't crush the human spirit creepy bitch mistress of cumalot dirty sexy Rachel": Labels.Frankenfics,
  "wrecking": Labels.Frankenfics,
  "Helium is way funnier than Polonium ow my ow": Labels.Cosima,
  "Did you threaten babies": Labels.Helena,
  "Your absence is inconvenient and your silence is irksome": Labels.Rachel
};

var unclassified = " ballpit evil broccoli titty malaysia" +
    " slick folds corncob vintage bordeaux cabernet crackship ship AU headcanon leather TDOOB severed tail dance" +
    " dental pulp harvest stem cells cloneswap cloning code genetic trials sequence" +
    //" pull tooth fatal kitchen where aliens horse cow" +
    //" meathead love" +
    " parking garage Iceland London Calling crazy science secrets are power loads silly tit twat heart forever" +
    " refund near-death experience Denise the cat eye patch Frankfurt craft room" +
    " fishsticks doodle holy grenade artificial womb selfish sacrifice pressed lips sick illness torture" +
    " holistic healer reiki Shay serial killer strong baby ox eff business proposition" +
    " There is only one way forward and this is it" +
    " Graeme John killed shot assassinated need want conspiracy give take am was were" +
    " Cosima manicure" +
    " as a lesbian supporter go sell a house";

var specialChars = "#.,\''!?".split("");
var suffixes     = ["ly", "ing", "fully", "ed", "s", "es", "'re", "'s", "y", "'ve"];
var dupWords     = "French French I I I I I'm I'm I'm me me me me me you you you you we we we us us us they they " +
    "and and and but but but for for because because while while " +
    "am was were are am was were are";
dupWords         = dupWords.split(" ");

//images:
//pencil
//pipe
// stick figure

var words = [
  "cytochrome C",
  "dirty sexy",
  "Je T'aime",
  "Atma Mitra",
  "Alpha Omega",
  "slick folds",
  "OBCrack",
  "#SaveDelphine",
  "French Jesus",
  "Big Dick Paul",
  "wrecking Pol",
  "Dr. Leekie",
  "..."
];

words = words.concat(specialChars, specialChars, suffixes, suffixes, suffixes, suffixes, dupWords);
words = words.concat(unclassified.split(" "));

var _          = require('underscore');
var mongo      = require('mongodb');
var monk       = require('monk');
var collection = monk('localhost:11111/ob-magnets').get("words");


var widthInInches = 36;
var heightInInches = 72;
var ppi = 72;

collection.remove({});

var sentWords = [];
Object.keys(sentences).forEach(function (sent) {
  //if (sentences[sent] === Labels.Leekie)
  sentWords = sentWords.concat(sent.split(" "));
});

words = words.concat(_.uniq(sentWords));

console.log(_.uniq(words).length)
console.log(words.length)


collection.insert(_.map(words, function (key, val) {
  return {
    fridge: 1,
    text: key,
    //speaker: val,
    x: Math.random() * widthInInches*ppi | 0,
    y: Math.random() * heightInInches*ppi | 0,
    type: 'plain',
    clicks: 0
  }
}));


var images = [
    "./img/AlisonBannerSmall.jpg",
    "./img/elect-alison.png",
    "./img/SRCLOGO.gif",
    "./img/familyPortrait.jpg",
    "./img/gunClub.png",
    "./img/alison-gun-magnet.png",
    "./img/bloodtiesSTORY.jpg",
    "./img/mod_03.gif",
    "./img/mod_06.gif",
    "./img/mod_09.gif",
    "./img/mod_13.gif",
    "./img/mod_17.gif",
    "./img/mod_21.gif",
    "./img/chevron_03.png",
    "./img/wecandoit.png"
];


collection.insert(_.map(images, function (key, val) {
  return {
    fridge: 1,
    text: key,
    x: Math.random() * widthInInches*ppi | 0,
    y: Math.random() * heightInInches*ppi| 0,
    type: 'image',
    clicks: 0
  }
}));

var bigLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

collection.insert(_.map(bigLetters, function (key, val) {
  return {
    fridge: 1,
    text: key,
    x: Math.random() * widthInInches*ppi | 0,
    y: Math.random() * heightInInches*ppi | 0,
    type: 'alphabet',
    clicks: 0
  }
}));