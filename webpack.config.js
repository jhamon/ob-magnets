var path    = require('path');
var webpack = require('webpack');

var webpackConfig = {
  entry: path.join(__dirname, 'assets/javascripts/main.js'),
  output: {
    path: path.join(__dirname, 'public/javascripts'),
    filename: "bundle.js"
  },
  module: {
    loaders: []
    //loaders: [{
    //  test: /\.jsx?$/,
    //  exclude: /(node_modules|bower_components)/,
    //  loader: 'babel'
    //}]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      mangle: {
        except: ['$super', '$', 'exports', 'require']
      }
    })
    //, new webpack.ProvidePlugin({
    //  // Automtically detect jQuery and $ as free var in modules
    //  // and inject the jquery library
    //  // This is required by many jquery plugins
    //  jQuery: "jquery",
    //  $: "jquery"
    //})
  ]
};

module.exports = webpackConfig;