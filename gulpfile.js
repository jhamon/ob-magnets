var gulp   = require('gulp');
var gutil  = require('gulp-util');
var del    = require('del');
var concat = require('gulp-concat');
var exec   = require('child_process').exec;
var path   = require('path');

// JavaScript plugins
var webpack       = require('webpack');
var webpackConfig = require('./webpack.config.js');
var uglify        = require('gulp-uglify');
var sourcemaps    = require('gulp-sourcemaps');

// CSS plugins
var sass   = require('gulp-sass');
var minify = require('gulp-minify-css');

var frontendJSFiles = [
  'assets/javascripts/*.js',
  'assets/javascripts/*/*.js',
  'assets/javascripts/*.jsx',
  'assets/javascripts/*/*.jsx'
];

var backendJSFiles = [
  '*.js',
  'src/*.js',
  'src/*/*.js',
  'routes/*.js',
  'routes/*/*.js'
];

gulp.task('clean:public', function () {
  del([
    'public/stylesheets/*',
    'public/javascripts/*'
  ])
});

gulp.task("build:js", function (callback) {
  webpack(webpackConfig
      , function (err, stats) {
        if (err) throw new gutil.PluginError("webpack", err);
        gutil.log("[webpack]", stats.toString({
          // output options
        }));
        callback();
      });
});

gulp.task('build:css', function () {
  var scss = [
    './vendors/foundation/scss/app.scss',
    './assets/stylesheets/styles.scss'
  ];

  gulp.src(scss)
      .pipe(sass())
      .pipe(minify())
      .pipe(concat('styles.min.css'))
      .pipe(gulp.dest('public/stylesheets/'))
});

gulp.task('start-app', function (cb) {
  exec('node app.js', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('watch:build', function () {
  gulp.watch('assets/stylesheets/*.scss', ['build:css']);
  gulp.watch('vendors/foundation/scss/foundation/_settings.scss', ['build:css']);
  gulp.watch(frontendJSFiles, ['build:js']);
});

gulp.task('run', [
  'clean:public',
  'build:js',
  'build:css',
  'watch:build',
  'start-app'
]);

gulp.task('default', ['run']);